//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.rydzewski.minecraft.chat;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;

public class ConfigManager {

    private FileConfiguration config;

    public ConfigManager(ChatPlugin plugin) {
        config = plugin.getConfig();
    }

    public int getMessageDelay() {
        return config.getInt("CoolDownMessage");
    }

    public String getServerName() {
        return config.getString("ServerName");
    }

    public List<String> getBannedWords() {
        return config.getStringList("BannedWords");
    }
}
