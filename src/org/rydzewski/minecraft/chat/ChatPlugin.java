package org.rydzewski.minecraft.chat;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class ChatPlugin extends JavaPlugin {

    static final String PERMISSION_CHAT_BYPASS = "chat.bypass";

    private ConfigManager data;
    private AtomicBoolean chatLocked = new AtomicBoolean(false);
    private Map<Player, Long> lastMessageTime = Collections.synchronizedMap(new HashMap<Player, Long>());

    public void onEnable() {
        saveDefaultConfig();
        data = new ConfigManager(this);
        new ChatCommand(this);
        new PlayerChatEvent(this, data.getBannedWords());
    }

    public ConfigManager getConfigManager() {
        return data;
    }

    public String colorString(String string) {
        return string.replaceAll("&([0-9a-z])", "§$1");
    }

    public boolean canSendMessage(Player player) {
        Long time = 0L;
        if(lastMessageTime.containsKey(player)) {
            time = lastMessageTime.get(player);
        }

        return player.hasPermission(PERMISSION_CHAT_BYPASS) || (System.currentTimeMillis() - time) / 1000L >= (long) data.getMessageDelay();
    }

    public void cleanChat() {
        for(int i = 0; i < 100; ++i) {
            if(i == 95) {
                getServer().broadcastMessage("" + ChatColor.GOLD + ChatColor.BOLD + "               Chat zostal wyczyszczony!");
            } else if(i == 94) {
                getServer().broadcastMessage(ChatColor.BOLD + "                       " + colorString(data.getServerName()));
            } else {
                getServer().broadcastMessage(" ");
            }
        }
    }

    public void toggleLockChat() {
        int i;
        if(isChatLocked()) {
            setChatLocked(false);

            for(i = 0; i < 10; ++i) {
                if(i == 7) {
                    getServer().broadcastMessage(ChatColor.BOLD + "               " + colorString("&6&lChat zostal &a&lodblokowany&6&l!"));
                } else if(i == 6) {
                    getServer().broadcastMessage(ChatColor.BOLD + "                       " + colorString(data.getServerName()));
                } else {
                    getServer().broadcastMessage(" ");
                }
            }
        } else {
            setChatLocked(true);

            for(i = 0; i < 10; ++i) {
                if(i == 7) {
                    getServer().broadcastMessage(ChatColor.BOLD + "               " + colorString("&6&lChat zostal &4&lzablokowany&6&l!"));
                } else if(i == 6) {
                    getServer().broadcastMessage(ChatColor.BOLD + "                       " + colorString(data.getServerName()));
                } else {
                    getServer().broadcastMessage(" ");
                }
            }
        }
    }

    public boolean isChatLocked() {
        return chatLocked.get();
    }

    private void setChatLocked(boolean value) {
        chatLocked.set(value);
    }

    public Map<Player, Long> getLastMessageTime() {
        return lastMessageTime;
    }
}
