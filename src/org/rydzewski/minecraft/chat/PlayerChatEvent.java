//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.rydzewski.minecraft.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.List;

public class PlayerChatEvent implements Listener {

    private ChatPlugin plugin;
    private String bannedPattern;

    public PlayerChatEvent(ChatPlugin plugin, List<String> bannedWords) {
        this.plugin = plugin;
        plugin.getServer().getPluginManager().registerEvents(this, plugin);

        boolean first = true;
        StringBuilder sb = new StringBuilder();

        sb.append(".*(");
        for (String bannedWord : bannedWords) {
            if (!first) {
                sb.append("|");
            }
            sb.append(bannedWord);
            first = false;
        }
        sb.append("+).*");
        bannedPattern = sb.toString();
    }

    @SuppressWarnings("UnusedDeclaration")
    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if(!player.hasPermission(ChatPlugin.PERMISSION_CHAT_BYPASS)) {
            String tempMessage = event.getMessage().toLowerCase();
            String dotted = "(.*[\\.,]{2,})|([\\.,]{2,}.*)";
            String url = ".*(http://)*(www)*[a-z-0-9]+(\\.)+[a-z-0-9]+.*";
            if(tempMessage.replaceAll("(\\s|_|\\W)+", "").matches(bannedPattern)) {
                event.setMessage("***");
            }

            if(!tempMessage.replaceAll(",", "").matches(dotted) && tempMessage.matches(url)) {
                event.setMessage("***");
            }
        }

        if(plugin.isChatLocked() && !player.hasPermission(ChatPlugin.PERMISSION_CHAT_BYPASS)) {
            player.sendMessage(ChatColor.BLUE + "Chat jest obecnie zablokowany!");
            event.setCancelled(true);
        } else {
            if(plugin.canSendMessage(player)) {
                plugin.getLastMessageTime().put(player, System.currentTimeMillis());
                return;
            }

            player.sendMessage(ChatColor.RED + "Wiadomosc na chacie mozesz wysylac co " + plugin.getConfigManager().getMessageDelay() + " sekund!");
            event.setCancelled(true);
        }
    }
}
