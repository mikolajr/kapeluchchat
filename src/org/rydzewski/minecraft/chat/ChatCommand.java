//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.rydzewski.minecraft.chat;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ChatCommand implements CommandExecutor {

    private ChatPlugin plugin;

    public ChatCommand(ChatPlugin plugin) {
        this.plugin = plugin;
        this.plugin.getCommand("chat").setExecutor(this);
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(args.length > 0) {
            if(args[0].equalsIgnoreCase("clear")) {
                plugin.cleanChat();
            } else if(args[0].equalsIgnoreCase("lock")) {
                plugin.toggleLockChat();
            } else if(args[0].equalsIgnoreCase("reload")) {
                plugin.reloadConfig();
                sender.sendMessage(ChatColor.GREEN + "Plik konfiguracji zostal przeladowany!");
            }
        } else {
            sender.sendMessage(ChatColor.BLUE + "Dostepne argumenty: " + ChatColor.RED + "clear, lock, reload");
        }

        return false;
    }
}
